package com.pavelperc.account

import org.amshove.kluent.shouldBeFalse
import org.amshove.kluent.shouldBeGreaterThan
import org.amshove.kluent.shouldBeTrue
import org.amshove.kluent.shouldEqual
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

/**
 * Примечание: Вы говорили, что после 3-го задания можно использовать любые библиотеки
 * для тестирования. Я решил попробовать JUnit 5 и kluent (для assertions, на котлине)
 * Исходный класс остался на java!
 */
class AccountTest {
    
    private var account = Account()
    
    @BeforeEach
    fun setUp() {
        account = Account()
    }
    
    @Test
    fun initialBalance() {
        account.balance shouldEqual 0
    }
    
    @Nested
    inner class WithdrawTests {
        
        @Test
        fun withdrawCorrect() {
            // also can use shouldBeFalse()
            account.isBlocked shouldEqual false
            
            // in kotlin, maxCredit is a generated 
            // readonly property from the method getMaxCredit, not field!!
            account.maxCredit shouldBeGreaterThan 10
            
            // shouldBeTrue is an extension function
            account.withdraw(10).shouldBeTrue()
            account.balance shouldEqual -10
        }
        
        // requirement 2
        // it fails!!!
        @Test
        fun withdrawEqualMaxCredit() {
            account.isBlocked.shouldBeFalse()
            account.balance shouldEqual 0
            
            account.block() // requirement 3
            account.setMaxCredit(10) shouldEqual true
            account.unblock().shouldBeTrue()
            
            account.withdraw(10).shouldBeTrue()
            account.balance shouldEqual -10
        }
        
        // requirement 2
        @Test
        fun withdrawOverMaxCredit() {
            account.isBlocked.shouldBeFalse()
            account.balance shouldEqual 0
    
            account.block() // requirement 3
            account.setMaxCredit(10) shouldEqual true
            account.unblock().shouldBeTrue()
            
            account.withdraw(11).shouldBeFalse()
            account.withdraw(12).shouldBeFalse()
            account.withdraw(10000).shouldBeFalse()
            account.balance shouldEqual 0
        }
        
        // requirement 7
        @Test
        fun withdrawNegative() {
            account.isBlocked.shouldBeFalse()
            account.balance shouldEqual 0
            
            account.withdraw(-1).shouldBeFalse()
            account.withdraw(-5).shouldBeFalse()
            account.withdraw(-10).shouldBeFalse()
            
            account.balance shouldEqual 0
        }
    
    
        // requirement 8
        @Test
        fun withdrawBlocked() {
            account.block()
            account.isBlocked.shouldBeTrue()
        
            account.balance shouldEqual 0
            account.withdraw(1).shouldBeFalse()
            account.balance shouldEqual 0
        }
    }
    
    @Nested
    inner class DepositTests {
        
        @Test
        fun depositSimple() {
            account.isBlocked.shouldBeFalse()
            account.balance shouldEqual 0
            
            account.deposit(10).shouldBeTrue()
            
            account.balance shouldEqual 10
        }
        
        // requirement 7
        @Test
        fun depositNegative() {
            account.isBlocked.shouldBeFalse()
            account.balance shouldEqual 0
            
            account.deposit(-1).shouldBeFalse()
            account.deposit(-2).shouldBeFalse()
            account.deposit(-5).shouldBeFalse()
            
            account.balance shouldEqual 0
        }
        
        // requirement 7
        @Test
        fun depositOverBound() {
            // actually bound should be static final and uppercase, but I didn't want to change hard the Account class
            val BOUND = account.bound
            
            account.isBlocked.shouldBeFalse()
            account.balance shouldEqual 0
            
            account.deposit(BOUND - 1).shouldBeTrue()
            account.balance shouldEqual BOUND - 1
            
            account.deposit(BOUND + 1).shouldBeFalse()
            account.balance shouldEqual BOUND - 1
        }
        
        // requirement 7
        @Test
        fun depositEqualBound() {
            // actually bound should be static final and uppercase, but I didn't want to change hard the Account class
            val BOUND = account.bound
            
            account.isBlocked.shouldBeFalse()
            account.balance shouldEqual 0
            
            account.deposit(BOUND).shouldBeTrue()
            account.balance shouldEqual BOUND
            
        }
    
        // requirement 8
        @Test
        fun depositBlocked() {
            account.block()
            account.isBlocked.shouldBeTrue()
            
            account.balance shouldEqual 0
            account.deposit(1).shouldBeFalse()
            account.balance shouldEqual 0
        }
    }
    
    
    
    @Nested
    inner class MaxCreditTests {
        
        // requirement 3
        // it fails!!!
        @Test
        fun maxCreditNonBlocked() {
            account.isBlocked.shouldBeFalse()
            
            val oldMaxCredit = account.maxCredit
            account.setMaxCredit(10).shouldBeFalse()
            account.setMaxCredit(20).shouldBeFalse()
            
            account.maxCredit shouldEqual oldMaxCredit
        }
    
        // requirement 5
        @Test
        fun maxCreditBlocked() {
            
            account.block()
            account.isBlocked.shouldBeTrue()
            
            account.bound = 30
            
            account.setMaxCredit(10).shouldBeTrue()
            account.maxCredit shouldEqual 10
            account.setMaxCredit(20).shouldBeTrue()
            account.maxCredit shouldEqual 20
            
            account.setMaxCredit(-20).shouldBeTrue()
            account.maxCredit shouldEqual -20
        }
    
        // requirement 4
        @Test
        fun maxCreditBound() {
            
            account.block()
            account.isBlocked.shouldBeTrue()
            
            account.bound = 30
            
            account.setMaxCredit(10).shouldBeTrue()
            account.maxCredit shouldEqual 10
            
            account.setMaxCredit(30).shouldBeTrue()
            account.maxCredit shouldEqual 30
            
            account.setMaxCredit(31).shouldBeFalse()
            account.maxCredit shouldEqual 30
            
            account.setMaxCredit(-30).shouldBeTrue()
            account.maxCredit shouldEqual -30
            
            account.setMaxCredit(-31).shouldBeFalse()
            account.maxCredit shouldEqual -30
        }
    }
    
    // requirement 6
    @Test
    fun unblock() {
        account.isBlocked shouldEqual false
    
        account.withdraw(10).shouldBeTrue()
        account.balance shouldEqual -10
        
        account.block()
        account.setMaxCredit(5).shouldBeTrue()
        
        // negative max credit is bigger then the balance
        account.unblock().shouldBeFalse()
        
        // less
        account.setMaxCredit(11).shouldBeTrue()
        account.unblock().shouldBeTrue()
        
        account.block()
        // equal
        account.setMaxCredit(10).shouldBeTrue()
        account.unblock().shouldBeTrue()
        
    }
    
    
    // requirement 6
    // with positive negative maxCredit!
    @Test
    fun unblock2() {
        account.isBlocked shouldEqual false
    
        account.deposit(10).shouldBeTrue()
        account.balance shouldEqual 10
        
        account.block()
        account.setMaxCredit(-15).shouldBeTrue()
        
        // positive negative max credit is bigger then the balance
        account.unblock().shouldBeFalse()
        
        // less
        account.setMaxCredit(-9).shouldBeTrue()
        account.unblock().shouldBeTrue()
        
        account.block()
        // equal
        account.setMaxCredit(-10).shouldBeTrue()
        account.unblock().shouldBeTrue()
        
    }
    
    @Test
    fun balanceOverflow() {
        account.isBlocked shouldEqual false
        
        val BOUND = account.bound
    
        while (Int.MAX_VALUE - account.balance > BOUND) {
            account.deposit(BOUND).shouldBeTrue()
        }
        
        account.balance shouldBeGreaterThan 0
        account.deposit(BOUND) // should be false because of overflow
        // overflow
        account.balance shouldBeGreaterThan 0
        
    }
    
}